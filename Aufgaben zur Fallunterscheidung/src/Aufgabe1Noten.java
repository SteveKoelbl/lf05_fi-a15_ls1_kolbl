import java.util.Scanner;

public class Aufgabe1Noten {

	public static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		byte noten;
		
		System.out.println("Bitte geben Sie eine Note ein: ");
		noten = tastatur.nextByte();
		
		if (noten == 1) {
			System.out.println("\nNote 1 = Sehr gut");
		}
		else if (noten == 2) {
			System.out.println("\nNote 2 = Gut");
		}
		else if (noten == 3) {
			System.out.println("\nNote 3 = Befriedigend");
		}
		else if (noten == 4) {
			System.out.println("\nNote 4 = Ausreichend");
		}
		else if (noten == 5) {
			System.out.println("\nNote 5 = Mangelhaft");
		}
		else if (noten == 6) {
			System.out.println("\nNote 6 = Ungen�gend");
		}
		else if (noten >= 7 || noten <= 0) {
			System.out.println("\nKeine akzebtable Note, bitte Eingabe �berpr�fen");
		}
	}

}
