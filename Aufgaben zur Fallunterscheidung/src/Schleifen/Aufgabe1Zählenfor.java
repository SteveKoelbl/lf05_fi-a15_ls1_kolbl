package Schleifen;

import java.util.Scanner;

public class Aufgabe1Z�hlenfor {

	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		int n;
		int i;
		char wahl;

		System.out.print(
				"M�chten Sie hoch oder runterz�hlen?\nSchreiben Sie eine 1 f�r hochz�hlen und eine 2 f�r runterz�hlen.");
		wahl = tastatur.next().charAt(0);

		if (wahl == '1') {
			System.out.print("Geben Sie ein bis zu welcher Zahl hochgez�hlt werden soll.\nZahl:");
			n = tastatur.nextInt();
			for (i = 1; i <= n; i++) {
				System.out.println(i);
			}
		} else if (wahl == '2') {
			System.out.print("Geben Sie ein Zahl ein, von welcher runtergez�hlt werden soll.\nZahl:");
			n = tastatur.nextInt();
			for (i = 1; i <= n; n--) {
				System.out.println(n);
			}
		}

	}
}
