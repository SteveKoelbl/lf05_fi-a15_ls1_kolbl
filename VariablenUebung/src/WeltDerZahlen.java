/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 100000000000l ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3766082 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6785 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000 ; 
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242 ;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);

    System.out.println("Alter in Tagen: " + alterTage);
    
    System.out.println("Schwerstes Tier in Kg: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr�ten Landes in km�: " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes in km�: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}


