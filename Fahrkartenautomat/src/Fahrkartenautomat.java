﻿import java.util.Scanner;

public class Fahrkartenautomat
{
	public static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
    	while (tastatur != null) {
            double zuZahlenderBetrag = fahrkartenbestellungErfassen(), ruckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            fahrkartenAusgeben();

            rueckgeldAusgeben(ruckgabebetrag);

            try {
                Thread.sleep(1000);
            } catch (Exception ignore) {}

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
        }

        tastatur.close();
}

    
    public static double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag;
        int mengeTickets = -1;

        String[] ticketN = new String[] { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB",
                "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };

        double[] ticketK = new double[] { 2.90, 3.30, 3.60, 1.90, 8.60, 9.0, 9.6, 23.5, 24.30, 23.90 };

        for (int i = 0; i < ticketN.length; i++) {
            System.out.printf("%s [%.2f EUR] (%d)%n", ticketN[i], ticketK[i], i + 1);
        }

        System.out.print("Ticket Auswahl: ");
        int selectedIndex = tastatur.nextInt();

        if (selectedIndex <= 0 || ticketN.length < (selectedIndex - 1)) {
            System.out.println("Fehlerhafter Eintrag!.");
            return fahrkartenbestellungErfassen();
        }

        zuZahlenderBetrag = ticketK[selectedIndex - 1];

        while (mengeTickets == -1) {
            System.out.print("Ticket Anzahl: ");
            
            // Integer bekommt einen Wert zugewiesen
            
            mengeTickets = tastatur.nextInt();
            if (mengeTickets <= 0 || mengeTickets > 10) {
                System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
                System.out.println();
                mengeTickets = -1;
            }
        }
    // Der zu Zahlende Betrag wird mit der Zahl der Tickets multipliziert 
    return zuZahlenderBetrag * (double) mengeTickets;
        }
        

public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	
	double eingezahlterGesamtbetrag;
	double eingeworfeneMünze;
	
	// Geldeinwurf
    // -----------
    eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    {
 	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " €");
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 €): ");
 	   eingeworfeneMünze = tastatur.nextDouble();
       eingezahlterGesamtbetrag += eingeworfeneMünze;
    }
	
    return eingezahlterGesamtbetrag - zuZahlenderBetrag; 

}

public static void fahrkartenAusgeben() {

	// Fahrscheinausgabe
    // -----------------
    // Wenn mehr als ein Fahrschein gekauft wurde ändert sich der Text
 	System.out.println("\n Ihre Fahrscheine werden ausgegeben");

    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
}

public static void rueckgeldAusgeben(double rückgabebetrag) {

    if(rückgabebetrag > 0.0)

    {
 	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }

    System.out.println("\nVergessen Sie nicht Ihre Fahrscheine\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
	
}
    
}