import java.util.Scanner;

public class JavaTrain {

	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau;
		char richtungHamburg;
		char haltInStendal;
		char endetIn;

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau
		
		// Halt in Spandau?
		System.out.print("Wollen Sie in Spandau halten? (j/n): ");
		haltInSpandau = tastatur.next().charAt(0);
		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}

		// Richtung Hamburg?
		System.out.print("Wollen Sie Richtung Hamburg? (j/n): ");
		richtungHamburg = tastatur.next().charAt(0);
		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96;
			System.out.println("Sie erreichten Hamburg nach " + fahrzeit + " Minuten.");
		} else {

			// Halt in Stendal?
			System.out.print("Wollen Sie in Stendal halten? (j/n): ");
			haltInStendal = tastatur.next().charAt(0);
			fahrzeit = fahrzeit + 34;
			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16;
			} else {
				fahrzeit = fahrzeit + 6;

				// Ende der Fahrt bestimmen
				System.out.print("Wo wollen Sie ankommen? (h/b/w): ");
				endetIn = tastatur.next().charAt(0);
				if (endetIn == 'h') {
					fahrzeit = fahrzeit + 62;
					System.out.println("Sie erreichten Hannover nach " + fahrzeit + " Minuten.");
				} else if (endetIn == 'b') {
					fahrzeit = fahrzeit + 50;
					System.out.println("Sie erreichten Braunschweig nach " + fahrzeit + " Minuten.");
				} else if (endetIn == 'w') {
					fahrzeit = fahrzeit + 29;
					System.out.println("Sie erreichten wolfsburg nach " + fahrzeit + " Minuten.");
				}
			}
		}
	}
}